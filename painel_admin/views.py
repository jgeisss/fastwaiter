# Painel Administrador - Juliano

from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

def index(request): 
	
	context = RequestContext(request)

	context_dict = {'boldmessage':"I'm from the context"}

	return render_to_response('painel_admin/index.html', context_dict, context)
